<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Apply for hospital readmission</name>
   <tag></tag>
   <elementGuidId>7f94c68e-ea77-411b-a1d8-dd7ea6286c6e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.checkbox-inline</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']/div/div/form/div[2]/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>c4e158fd-d7c3-4d6a-9ea2-564bcf5b0b88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>chk_hospotal_readmission</value>
      <webElementGuid>120d3536-d08f-40f9-9f1f-ea291e5f70f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkbox-inline</value>
      <webElementGuid>77298eb3-46a8-4a60-b688-ff6cd6572d6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                             Apply for hospital readmission
                        </value>
      <webElementGuid>5d0c6eb1-8c02-49e3-bc0d-33cab29d4a7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/form[@class=&quot;form-horizontal&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;col-sm-offset-5 col-sm-4&quot;]/label[@class=&quot;checkbox-inline&quot;]</value>
      <webElementGuid>11274df3-687a-4fe4-a576-70f1dc3971ab</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[2]/div/label</value>
      <webElementGuid>ca354cc8-c757-421d-b465-92e8b9bdee7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Facility'])[1]/following::label[1]</value>
      <webElementGuid>3211466d-7725-4221-8cfc-5a4ce0e2eb50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[2]/following::label[2]</value>
      <webElementGuid>22e6af23-7531-4cc3-9c46-298f43904359</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Healthcare Program'])[1]/preceding::label[1]</value>
      <webElementGuid>3e883060-e674-4a70-b3bd-f03dd4ac8100</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Apply for hospital readmission']/parent::*</value>
      <webElementGuid>08ac2dea-84fb-4d5c-a0a6-911ce25e2c20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/label</value>
      <webElementGuid>03397157-a824-40fa-adc7-733b9ddb2c03</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = '
                             Apply for hospital readmission
                        ' or . = '
                             Apply for hospital readmission
                        ')]</value>
      <webElementGuid>2bbbc434-d89e-46e2-8d6c-78edefd3eca6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
